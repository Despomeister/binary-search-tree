import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.lang.Math;

public class Main {
    static int ilosc_elementow_drzewa = 0;
    static List<Integer>elementy_drzewa_bst = new LinkedList();
    static int aktualna_wysokosc_drzewa = 0;
    static int ostateczna_wysokosc_drzewa = 0;
    static int element_minimalny_drzewa;
    static int ilosc_operacji_do_maks = 0;
    static int element_maksymalny_drzewa;
    static int ilosc_operacji_do_min = 0;
    static List<Integer>klucze_drzewa_bst = new LinkedList<>();
    static Wezel korzen = null;
    static Wezel newKorzen = null;
    static int zakreCalkowity = 0;
    public static final Random gen = new Random();

    public static void main(String[] args) throws IOException
    {
        //Pobieranie wartosci z pliku z pdf'a
        String range = Files.readAllLines(Paths.get("file.txt")).get(0);
        String randToAdd = Files.readAllLines(Paths.get("file.txt")).get(1);
        String randToDelete = Files.readAllLines(Paths.get("file.txt")).get(2);
        String elemToRotate = Files.readAllLines(Paths.get("file.txt")).get(3);

        System.out.println("Elementy wczytane z pliku: \n"+"Zakres do: "+range+" \nIlosc losowych elementow do dodania: "+randToAdd+
                " \nIlosc losowych elementow do usuniecia: "+randToDelete+" \nDane do rotacji: "+elemToRotate+"\n");

        System.out.println("\n* * * * * PIERWOTNE DRZEWO BST * * * * *\n\n");
        //Tworzenie listy elementow
        int zakres = Integer.parseInt(range);
        //Losowanie podstawowych elementow naszego drzewa
        for (int i : Generator_liczb_bez_powtorzen(zakres,100))
            elementy_drzewa_bst.add(i);
        System.out.println("Wylosowane elementy drzewa bst to: " + elementy_drzewa_bst);

        //Przypisanie pierwszego elementu listy do korzenia
        if(korzen == null)
        {
            korzen = new Wezel(elementy_drzewa_bst.get(0));
            newKorzen = new Wezel(elementy_drzewa_bst.get(0));
        }
        System.out.println("\nKorzen wynosi: " + korzen.klucz);

        //Inicjalizacja elementow maksymalnych i minimalnych
        element_minimalny_drzewa = elementy_drzewa_bst.get(0);
        element_maksymalny_drzewa = elementy_drzewa_bst.get(0);

        //Tworzenie drzewa
        for(int i = 1; i < zakres; i++) {
            aktualna_wysokosc_drzewa = 0;
            Tworzenie_Drzewa_BST(korzen, elementy_drzewa_bst.get(i), i);

            //Wysokosc drzewa
            if(ostateczna_wysokosc_drzewa < aktualna_wysokosc_drzewa)
                ostateczna_wysokosc_drzewa = aktualna_wysokosc_drzewa;
        }

        System.out.println("\n------------------------------------------------------------");
        //Rysowanie drzewa
        System.out.println("Rysunek BST: ");
        if(korzen.klucz > elementy_drzewa_bst.get(ilosc_elementow_drzewa))
            Drukowanie_drzewa_BST("             ",korzen,true);
        else
            Drukowanie_drzewa_BST("             ",korzen,false);
        System.out.println();

        System.out.println("------------------------------------------------------------");
        //Wysokosc drzewa
        ostateczna_wysokosc_drzewa++;
        System.out.println("\nWysokosc drzewa to: " + ostateczna_wysokosc_drzewa+"\nLiczba elementow z ktorych zbudowane jest drzewo to: " + elementy_drzewa_bst.size());

        //Wypisanie elementu maksymalnego, minimalnego oraz wszystkich przeszukiwan dla aktualnego drzewa
        daneAktualnegoDrzewa(korzen);
        System.out.println("\n\n------------------------------------------------------------");

        //Losowanie dodatkowych elementow do listy - ilosc elementow na podstawie informacji z pliku
        System.out.println("\n\n* * * * * DRZEWO BST PO DODANIU NOWYCH ELEMENTOW * * * * *\n");
        int ilosc_dodatkowych_liczb = Integer.parseInt(randToAdd);
        zakreCalkowity = ilosc_dodatkowych_liczb + zakres;
        System.out.println("\nIlosc losowych elementow do dodania: "+randToAdd);
        System.out.println("Dodatkowo wylosowane liczby to: ");
        //Losujemy dodatkowe liczby by je dopisac do naszej listy kluczy drzewa BST
        for (int i : Generator_liczb_bez_powtorzen(ilosc_dodatkowych_liczb,100))
        {
            System.out.println("Dodany element to: " + i);
            //Sprawdzamy czy w naszym dotychczasowym zbiorze istnieje juz element ktory wylosowalismy na nowo
            if(czyMamyJuzTakiElement(elementy_drzewa_bst, i))
            {
                System.out.println("Element " + i + " juz istnieje w naszym zbiorze, losujemy nowy w jego miejsce.");
                //Przypisujemy zmiennej a wartosc pierwszego elementu naszego drzewa
                int a = elementy_drzewa_bst.get(0);
                //Wykonujemy dopoki nasza zmienna a nie bedzie nalezec do naszej aktualnej listy
                while(czyMamyJuzTakiElement(elementy_drzewa_bst, a))
                {
                    a = ThreadLocalRandom.current().nextInt(0, 100 + 1);
                }
                System.out.println("Nowy element to: " + a);
                elementy_drzewa_bst.add(a);
            }
            else
            {
                elementy_drzewa_bst.add(i);
            }
        }

        System.out.println("\nAktualna lista elementow drzewa bst to: " + elementy_drzewa_bst);

        //Tworzenie nowego drzewa
        for(int i = 1; i < zakreCalkowity; i++) {
            aktualna_wysokosc_drzewa = 0;
            Tworzenie_Drzewa_BST(newKorzen, elementy_drzewa_bst.get(i), i);

            //Wysokosc drzewa
            if(ostateczna_wysokosc_drzewa < aktualna_wysokosc_drzewa)
                ostateczna_wysokosc_drzewa = aktualna_wysokosc_drzewa;
        }

        System.out.println("\n------------------------------------------------------------");

        //Rysowanie drzewa
        System.out.println("\nRysunek BST po dodaniu nowych losowo wygenerowanych elementow: \n\n");
        Rysuj(newKorzen,zakreCalkowity);
        System.out.println("\n------------------------------------------------------------");

        //Wysokosc drzewa
        ostateczna_wysokosc_drzewa++;
        System.out.println("\nWysokosc drzewa to: " + ostateczna_wysokosc_drzewa+"\nLiczba elementow z ktorych zbudowane jest drzewo to: " + elementy_drzewa_bst.size());

        //Wypisanie elementu maksymalnego, minimalnego oraz wszystkich przeszukiwan dla aktualnego drzewa
        ilosc_operacji_do_min = 0;
        ilosc_operacji_do_maks = 0;
        daneAktualnegoDrzewa(newKorzen);
        System.out.println("\n\n------------------------------------------------------------");

        //Rozpoczecie interakcji z uzytkownikiem
        String znak = "";
        while(!znak.equals("t") || newKorzen != null) {
            Scanner scan = new Scanner(System.in);
            System.out.println("\nDostepne operacje na aktualnym drzewie BST: ");
            System.out.println("1 - Wyszukiwanie wybranego elementu w drzewie BST.");
            System.out.println("2 - Rotacja wybranego przez nas wezla w aktualnym drzewie BST.");
            System.out.println("3 - Balans naszego drzewa BST.");
            System.out.println("4 - Usuwanie wybranego wezla w aktualnym drzewie BST.");
            System.out.println("DEFAULT - Koniec programu. (Dowolnie wpisana cyfra)");
            System.out.print("Wybieram: ");
            String character = scan.nextLine();
            int number = Integer.parseInt(character);
            switch (number)
            {
                //Wyszukiwanie podanego przez nas elementu w naszym drzewie
                case 1:
                    System.out.println("\n\n* * * * * WYSZUKIWANIE ELEMENTU DRZEWA BST * * * * *\n");
                    System.out.println("Elementy naszego drzewa BST to: " + elementy_drzewa_bst);
                    System.out.print("Podaj klucz elementu drzewa BST jaki chcesz odnalezc: ");
                    Scanner scan1 = new Scanner(System.in);
                    String character1 = scan1.nextLine();
                    int number1 = Integer.parseInt(character1);
                    System.out.println("Podany przez ciebie klucz to: " + number1);
                    znajdzWezel(newKorzen,number1);
                    break;
                //Rotacja w lewo lub w prawo wzgledem podanego przez nas wezla (Jesli to mozliwe)
                case 2:
                    System.out.println("\n\n* * * * * ROTACJA ELEMENTU DRZEWA BST * * * * *\n");
                    System.out.println("Elementy naszego drzewa BST to: " + elementy_drzewa_bst);
                    System.out.println("\n------------------------------------------------------------");
                    System.out.println("\nRysunek drzewa BST gotowego do wykonania rotacji: \n\n");
                    Rysuj(newKorzen,zakreCalkowity);
                    System.out.println("\n------------------------------------------------------------");
                    System.out.println("W ktora strone chcesz wykonac rotacje?");
                    System.out.println("1 - w lewo");
                    System.out.println("2 - w prawo");
                    System.out.println("Defaul - koniec");
                    System.out.print("Wybieram: ");
                    Scanner scan2 = new Scanner(System.in);
                    String character2 = scan2.nextLine();
                    int wyborRotacji = Integer.parseInt(character2);
                    switch (wyborRotacji)
                    {
                        //Rotacja lewo
                        case 1:
                            System.out.print("Podaj klucz elementu drzewa BST wzgledem ktorego chcesz wykonac rotacje: ");
                            Scanner scan3 = new Scanner(System.in);
                            String character3 = scan3.nextLine();
                            boolean czyIstnieje = false;
                            int wybor1 = Integer.parseInt(character3);
                            //Sprawdzamy czy podany przez nas klucz nalezy do naszego drzewa BST
                            for(int i: elementy_drzewa_bst)
                            {
                                if (i == wybor1) {
                                    czyIstnieje = true;
                                    break;
                                }
                                else
                                    czyIstnieje = false;

                            }
                            //Jesli tak to wykonujemy rotacje (lewo)
                            if(czyIstnieje){
                                System.out.println("Podany przez ciebie klucz to: " + wybor1);
                                System.out.println("\n------------------------------------------------------------");
                                rotacjaLewa(wybor1);
                            }
                            else
                                System.out.println("Wezel o podanym przez ciebie kluczu nie istnieje w naszym drzewie BST.");
                            break;
                        //Rotacja prawo
                        case 2:
                            System.out.print("Podaj klucz elementu drzewa BST wzgledem ktorego chcesz wykonac rotacje: ");
                            Scanner scan4 = new Scanner(System.in);
                            String character4 = scan4.nextLine();
                            boolean czyIstnieje2 = false;
                            int wybor2 = Integer.parseInt(character4);
                            //Sprawdzamy czy podany przez nas klucz nalezy do naszego drzewa BST
                            for(int i: elementy_drzewa_bst)
                            {
                                if (i == wybor2) {
                                    czyIstnieje2 = true;
                                    break;
                                }
                                else
                                    czyIstnieje2 = false;

                            }
                            //Jesli tak to wykonujemy rotacje (prawo)
                            if(czyIstnieje2){
                                System.out.println("Podany przez ciebie klucz to: " + wybor2);
                                System.out.println("\n------------------------------------------------------------");
                                rotacjaPrawa(wybor2);
                            }
                            else
                                System.out.println("Wezel o podanym przez ciebie kluczu nie istnieje w naszym drzewie BST.");
                            break;
                        //Jesli wpiszemy dowolnie inna liczbe niz 1 i 2 to przerywamy operacje rotacji
                        default:
                            System.out.println("\nOperacja rotacji przerwana!");
                            break;
                    }
                    break;
                //Wykonywanie balansu aktualnego drzewa BST
                case 3:
                    System.out.println("\n\n* * * * * BALANS NASZEGO DRZEWA BST * * * * *\n");
                    DSW();
                    System.out.println("\n\n* * * * * ZBALANSOWANE PRZEZ NAS DRZEWO * * * * *\n");
                    Rysuj(newKorzen,zakreCalkowity);
                    break;
                //Usuwanie dowolnego elementu naszego drzewa BST
                case 4:
                    System.out.println("\n\n* * * * * USUWANIE ELEMENTU DRZEWA BST * * * * *\n\n");
                    System.out.println("Elementy naszego drzewa BST to: " + elementy_drzewa_bst);
                    System.out.print("Podaj klucz elementu drzewa BST do usuniecia: ");
                    Scanner scan5 = new Scanner(System.in);
                    String character5 = scan5.nextLine();
                    int number5 = Integer.parseInt(character5);
                    System.out.println("Podany przez ciebie klucz to: " + number5);
                    boolean czyIstnieje3 = false;
                    //Sprawdzamy czy podany przez nas klucz nalezy do naszego drzewa BST
                    for(int i: elementy_drzewa_bst)
                    {
                        if (i == number5) {
                            czyIstnieje3 = true;
                            break;
                        }
                        else
                            czyIstnieje3 = false;

                    }
                    //Jesli tak to usuwamy nasz element
                    if(czyIstnieje3)
                    {
                        System.out.println("Podany przez ciebie klucz do usuniecia to: " + number5);
                        System.out.println("\n------------------------------------------------------------");
                        newKorzen = usuwanieWezla(newKorzen, number5);
                        //Jesli nasz korzen nie jest pusty to go rysujemy
                        if(newKorzen != null)
                            Rysuj(newKorzen,zakreCalkowity);
                        else
                        {
                            System.out.println("Nasze drzewo jest puste!");
                            break;
                        }
                    }
                    else
                        System.out.println("Wezel o podanym przez ciebie kluczu nie istnieje w naszym drzewie BST.");
                    break;
                //Dowolnie inna cyfra niz 1..4 - Konczymy dzialanie programu
                default:
                    System.out.println("\n\n* * * * * KONIEC PROGRAMU * * * * *\n");
                    znak = "t";
                    break;
            }
        }
        System.out.println("Konczymy na dzis!");
    }

    //Metoda sprawdzajaca czy w naszej tablicy elementow drzewa BST sa juz wezly o takim kluczu
    private static boolean czyMamyJuzTakiElement(List<Integer> tablica, int elementDoSprawdzenia)
    {
        boolean test = false;
        for (int element : tablica) {
            if (element == elementDoSprawdzenia) {
                test = true;
                break;
            }
        }
        return test;
    }

    //Metoda rysujaca nasze aktualne drzewo
    public static void Rysuj(Wezel newKorzen ,int zakreCalkowity)
    {
        //Rysowanie drzewa
        if(newKorzen.klucz > elementy_drzewa_bst.get(zakreCalkowity-1))
            Drukowanie_drzewa_BST("             ",newKorzen,true);
        else
            Drukowanie_drzewa_BST("             ",newKorzen,false);
    }

    //Metoda tworzenia naszego drzewa BST
    public static Wezel Tworzenie_Drzewa_BST(Wezel korzen, int klucz,int i)
    {
        //Pierwszy element traktujemy jako korzen
        if (korzen == null) {
            korzen = new Wezel(elementy_drzewa_bst.get(i));
            return korzen;
        }
        //Porownujemy wartosc nastepnie dodanego elementu z kluczem korzenia
        //Jesli klucz nastepnego elementu jest mniejszy lub rowny wartosci klucza korzenia to umieszczamy go z lewej strony korzenia
        if (korzen.klucz >= klucz) {
            //Jesli kolejny element czyli przykladowo juz trzeci jest mniejszy od korzenia i drugiego elementu to rekrurencyjnie umieszczamy go po lewej
            korzen.lewy = Tworzenie_Drzewa_BST(korzen.lewy, klucz, i);
        }
            //Jesli nastepny element jest wiekszy od korzenia to idzie na prawo
            //Jesli jest wiekszy od kolejnego elementu to rekurencyjnie wrzucamy go jako prawego syna
            //Jesli element bedzie wiekszy od korzenia i przykladowo mniejszy od jego prawgo syna to staje sie lewym synem, prawego syna korzenia
        else {
            korzen.prawy = Tworzenie_Drzewa_BST(korzen.prawy, klucz,i);
        }

        //Liczenie wysokosc drzewa
        aktualna_wysokosc_drzewa++;

        //Porowynywanie aktualnie maksymalnego elementu z nowym kluczem i ewentualna podmiana
        if(element_maksymalny_drzewa < klucz)
            element_maksymalny_drzewa = klucz;

        //Porowynywanie aktualnie minimalnego elementu z nowym kluczem i ewentualna podmiana
        if(element_minimalny_drzewa > klucz)
            element_minimalny_drzewa = klucz;

        //Czynnosci powtarzamy az skoncza nam sie kolejne elementy do porownywania i zaczynamy dodawac kolejny element i go porownywac
        return korzen;
    }

    //Metoda wykonywana wewnatrz metody "Rysuj". Tworzy nasz rysunek
    //Inspirowane ze strony StackOverflow
    public static void Drukowanie_drzewa_BST(String przedrostek, Wezel korzen, boolean czy_istnieje_lewy_syn)
    {
        if (korzen != null)
        {
            Drukowanie_drzewa_BST(przedrostek + "     ", korzen.prawy, false);
            if(czy_istnieje_lewy_syn)
                System.out.println (przedrostek + ("\\-- ") + korzen.klucz);
            else
                System.out.println (przedrostek + ("/-- ") + korzen.klucz);
            Drukowanie_drzewa_BST(przedrostek + "     ", korzen.lewy, true);
        }
    }

    //Przeszukiwanie metoda Preorder naszego aktualnego drzewa
    public static void Przeszukiwanie_Preorder(Wezel korzen)
    {
        if (korzen == null)
            return;
        //Zaczynamy od korzenia
        System.out.print(korzen.klucz + " ");
        //Idziemy caly czas w lewo
        //Jesli nie ma wiecej w lewo to idziemy w prawo
        Przeszukiwanie_Preorder(korzen.lewy);
        //Rysujemy prawy
        //Jesli jest syn to rysujemy dalej w prawo badz w lewo do konca lewej galezi
        //Jesli nie ma zadnego syna to wracamy do poprzedniego wezla i sprawdzamy jego prawego syna
        Przeszukiwanie_Preorder(korzen.prawy);
        //Jak przejdziemy cala lewa galaz to robimy wszystko to samo analogicznie z prawa galezia az wszystkie dane beda zuzyte
    }

    //Przeszukiwanie metoda Inorder naszego aktualnego drzewa
    public static void Przeszukiwanie_Inorder(Wezel korzen)
    {
        if (korzen == null)
            return;
        //Zaczynam od wypisania maksymalnego elementu na lewo, jesli ma on prawego syna to nastepnie wypisujemy ostatniego lewego syna z prawej galezi
        //Nastepnie wypisujemy od samego dolu rodzica
        //Jesli ma on prawego syna to wypisujemy go
        //Czynnosci te powtarzamy az wrocimy do korzenia
        //Gdy jestesmy przy korzeniu i go wypiszemy zaczynamy z prawa galezia i od pierwszego napotkanego wezla schodzimy caly czas w lewo do liscia
        //Gdy juz mam liscia albo go poprostu nie ma to rysujemy jego rodzica i ew jego prawego syna az wrocimy do wezla z ktorego startowalismy
        //Nastepnie idziemy w prawo i sprawdzamy kolejny wezel w taki sam sposob az dojdziemy do prawego liscia ostatniego wezla czyli az wszystkie elementy zostana wypisane
        Przeszukiwanie_Inorder(korzen.lewy);
        System.out.print(korzen.klucz + " ");
        Przeszukiwanie_Inorder(korzen.prawy);
    }

    //Przeszukiwanie metoda Postorder naszego aktualnego drzewa
    public static void Przeszukiwanie_Postorder(Wezel korzen)
    {
        if (korzen == null)
            return;
        //Schodzimy maksymalnie w dol lewej galezi
        //Jesli ostatni rodzic ma lewego syna (lisc) to go wypisujemy, jesli nie to prawy
        //Nastepnie wypisujemy rodzica i patrzymy jakim jest lisciem swojego rodzica
        //Jesli lewym to wypisujemy lisc prawy i nastepnie rodzica
        //Jesli prawym to wypisujemy rodzica i idziemy w gore drzewa az do korzenia, lecz go nie wypisujemy
        Przeszukiwanie_Postorder(korzen.lewy);
        //Nastepnie rozpatrujemy prawa galaz
        //Schodzimy maksymalnie na lewo i wypisujemy lewego liscia
        //Jesli ma prawego i on nie ma dzieci to go wypisujemy i jego rodzica
        //Jesli prawy syn ma dzieci to schodzimy do jego najnizszego lewego syna i go wypsiujemy po czym wracamy do gory
        //Gdy juz sprawdzimy cala lewa galas pierwszego wezla to nie wypsiujemy go i sprawdzamy prawa galaz i analogicznie jak wczesniej
        //zaczynam od lewych elementow i prawych az dojdziemy do ostatniego elementu na prawo czyli prawego liscia
        //Po czym zaczynamy wracac do korzenia wypisujac wszystkich rodzicow ktorych wczesniej pominelismy
        Przeszukiwanie_Postorder(korzen.prawy);
        //Na samym koncu wypisujemy korzen
        System.out.print(korzen.klucz + " ");
    }

    //Generator losujacy liczby z podanego przez nas zakresu bez powtorze
    //Wykorzystany do tworzenia listy kluczy drzewa BST oraz do dodawania nowych elementow drzewa
    //Generator wziety z internetu
    public static int[] Generator_liczb_bez_powtorzen(int n, int maxRange) {
        assert n <= maxRange;
        int[] result = new int[n];
        Set<Integer> used = new HashSet<Integer>();
        for (int i = 0; i < n; i++) {
            int newRandom;
            do {
                newRandom = gen.nextInt(maxRange+1);
            } while (used.contains(newRandom));
            result[i] = newRandom;
            used.add(newRandom);
        }
        return result;
    }

    //Metoda zwracajaca najmniejszy klucz naszego drzewa
    static int elementMinimalnyDrzewaBST(Wezel korzen)
    {
        //Inicjalizacja zmiennej
        Wezel aktualnie_sprawdzany_korzen = korzen;
        //Petla sie wykonuje dopoki mamy lewego syna
        while (aktualnie_sprawdzany_korzen.lewy != null)
        {
            //Naszym aktualnym wezlem staje sie nasz aktualnie lewy syn
            aktualnie_sprawdzany_korzen = aktualnie_sprawdzany_korzen.lewy;
            //Za kazdym razem gdy mamy lewego syna liczba operacji + 1
            ilosc_operacji_do_min++;
        }
        //Zwracamy wartosc najmniejszego klucza w naszym drzewie
        return (aktualnie_sprawdzany_korzen.klucz);
    }

    //Metoda zwracajaca najwiekszy klucz naszego drzewa
    static int elementMaksymalnyDrzewaBST(Wezel korzen)
    {
        //Inicjalizacja zmiennej
        Wezel aktualnie_sprawdzany_korzen = korzen;
        //Petla sie wykonuje dopoki mamy prawego syna
        while (aktualnie_sprawdzany_korzen.prawy != null)
        {
            //Naszym aktualnym wezlem staje sie nasz aktualnie prawy syn
            aktualnie_sprawdzany_korzen = aktualnie_sprawdzany_korzen.prawy;
            //Za kazdym razem gdy mamy prawego syna liczba operacji + 1
            ilosc_operacji_do_maks++;
        }
        //Zwracamy wartosc najwiekszego klucza w naszym drzewie
        return (aktualnie_sprawdzany_korzen.klucz);
    }

    //Metoda wypisujaca informacji na temat naszego aktualnego drzewa BST - elem min, elem maks, wszystkie 3 przeszukiwania
    static void daneAktualnegoDrzewa(Wezel korzen)
    {
        //Element minimalny
        System.out.println("\nElement minimalny w tym drzewie to: " + elementMinimalnyDrzewaBST(korzen) + " Ilosc operacji: " + ilosc_operacji_do_min);
        //Element maksymalny
        System.out.println("\nElement maksymalny w tym drzewie to: " + elementMaksymalnyDrzewaBST(korzen) + " Ilosc operacji: " + ilosc_operacji_do_maks);
        //Przeszukiwania
        System.out.println("\nPrzeszukiwanie metoda Preorder: ");
        Przeszukiwanie_Preorder(korzen);
        System.out.println("\n\nPrzeszukiwanie metoda Inorder: ");
        Przeszukiwanie_Inorder(korzen);
        System.out.println("\n\nPrzeszukiwanie metoda Postorder: ");
        Przeszukiwanie_Postorder(korzen);
    }

    //Metoda ktora sprawdza czy wprowadzony przez nas klucz isnieje w naszym drzewie
    //Jesli tak to wypisuje sciezke jaka dostaniemy sie do niego
    static void znajdzWezel(Wezel korzen, int szukanyKlucz)
    {
        //Lista kluczy - potrzebna do wypisania sciezki
        klucze_drzewa_bst.clear();
        //Inicjalizacja
        Wezel aktualnie_sprawdzany_korzen = korzen;

        //Petla wykonujaca sie dopoki nie znajdziemy naszego klucza
        while (aktualnie_sprawdzany_korzen.klucz != szukanyKlucz)
        {
            //Za kazdym razem jak wejdziemy do petli to dodajemy aktualnie sprawdzany przez nas klucz do listy
            klucze_drzewa_bst.add(aktualnie_sprawdzany_korzen.klucz);
            //Jesli nasz klucz jest mniejszy od szukanego klucza
            if(aktualnie_sprawdzany_korzen.klucz < szukanyKlucz)
            {
                //I jesli mamy prawego syna
                if(aktualnie_sprawdzany_korzen.prawy != null)
                    //To nasz aktualny wezel jest rowny jego prawemu synowi
                aktualnie_sprawdzany_korzen = aktualnie_sprawdzany_korzen.prawy;
                //Jesli nie mamy prawgo syna to nie ma szukanego przez nas klucza w naszym drzewie BST
                else
                {
                    System.out.println("Nie ma takiego wezla");
                    return;
                }
            }
            //Jesli nasz klucz jest wiekszy od szukanego przez nas klucza
            else if(aktualnie_sprawdzany_korzen.klucz > szukanyKlucz)
            {
                //I jesli mamy lewego syna
                if(aktualnie_sprawdzany_korzen.lewy != null)
                    //To nasz aktualny wezel jest ronwy jego lewemu synowi
                aktualnie_sprawdzany_korzen = aktualnie_sprawdzany_korzen.lewy;
                //Jesli nie mamy lewego syna to nie ma szukanego przez nas klucza w naszym drzewie BST
                else
                {
                    System.out.println("Nie ma takiego wezla");
                    return;
                }
            }
        }
        //Jesli znalezlismy szukany przez nas klucz w naszym drzewie BST
        if(aktualnie_sprawdzany_korzen.klucz == szukanyKlucz)
        {
            System.out.println("\nUdalo sie! Znalezilismy element o kluczu: " + aktualnie_sprawdzany_korzen.klucz + " w naszym drzewie BST!");
            //Jezeli lista jest pusta to znaczy ze szukany przez nas klucz jest korzeniem
            if(klucze_drzewa_bst.size() == 0)
                System.out.println("To jest korzen.");
            //Jesli nie jest pusta
            else
            {
                //To wypisujemy wszystkie elementy naszej listy
                //Jest to sciezka do naszego klucza
                System.out.print("Sciezka do naszego wezla: ");
                for(int i: klucze_drzewa_bst) {
                    System.out.print("-" + i);
                }
                System.out.println("");
            }

        }
    }

    private static void rotacjaLewa(int klucz) {
        Wezel aktualnie_sprawdzany_korzen = tworzenieKopii(znajdzWezel(klucz));
        Wezel aktualny_rodzic_sprawdzanego_korzenia = znajdzRodzica(aktualnie_sprawdzany_korzen);
        Wezel aktualny_dziadek_sprawdzanego_korzenia = znajdzRodzica(aktualny_rodzic_sprawdzanego_korzenia);
        Wezel przyszly_rodzic = null;

        if(aktualny_rodzic_sprawdzanego_korzenia != null) {
            if (aktualny_rodzic_sprawdzanego_korzenia.prawy.klucz == aktualnie_sprawdzany_korzen.klucz) {
                if (aktualnie_sprawdzany_korzen.klucz == klucz) {
                    System.out.println("Szukany przez nas wezel: " + aktualnie_sprawdzany_korzen.klucz);
                    System.out.println("Rodzic naszego wezla: " + aktualny_rodzic_sprawdzanego_korzenia.klucz);
                    przyszly_rodzic = aktualnie_sprawdzany_korzen;    // p = A
                    Wezel x = aktualnie_sprawdzany_korzen.lewy;             // x = E
                    przyszly_rodzic.lewy = aktualny_rodzic_sprawdzanego_korzenia; // lewy od A = B
                    aktualny_rodzic_sprawdzanego_korzenia.prawy = x; // prawy od B = E

                    if (aktualny_dziadek_sprawdzanego_korzenia != null) {
                        System.out.println("Dziadek naszego wezla: " + aktualny_dziadek_sprawdzanego_korzenia.klucz);
                        if (aktualny_dziadek_sprawdzanego_korzenia.lewy == aktualny_rodzic_sprawdzanego_korzenia)
                            aktualny_dziadek_sprawdzanego_korzenia.lewy = przyszly_rodzic;
                        else aktualny_dziadek_sprawdzanego_korzenia.prawy = przyszly_rodzic;
                    }

                    if (aktualny_dziadek_sprawdzanego_korzenia == null)
                        newKorzen = przyszly_rodzic;
                }
                System.out.println("------------------------------------------------------------");
                System.out.println("\nRysunek BST po wykonaniu rotacji w lewo wzgledem wezla o kluczu rownym " + aktualnie_sprawdzany_korzen.klucz + ": \n\n");
                Rysuj(newKorzen, zakreCalkowity);
                System.out.println("\n------------------------------------------------------------");
            } else {
                System.out.println("\nPrzy uzyciu tego wezla nie mozna wykonac rotacji w lewo!");
                System.out.println("\n------------------------------------------------------------");
            }
        }
        else{
            System.out.println("Szukany przez nas wezel: " + aktualnie_sprawdzany_korzen.klucz);
            przyszly_rodzic = aktualnie_sprawdzany_korzen.prawy; // A = C
            Wezel x = aktualnie_sprawdzany_korzen.prawy.lewy; // F lewy od A
            przyszly_rodzic.lewy = aktualnie_sprawdzany_korzen;
            aktualnie_sprawdzany_korzen.prawy = x;
            newKorzen = przyszly_rodzic;
            System.out.println("------------------------------------------------------------");
            System.out.println("\nRysunek BST po wykonaniu rotacji w prawo wzgledem wezla o kluczu rownym " + aktualnie_sprawdzany_korzen.klucz + ": \n\n");
            Rysuj(newKorzen,zakreCalkowity);
            System.out.println("\n------------------------------------------------------------");
        }
    }

    private static void rotacjaPrawa(int klucz)
    {
        Wezel aktualnie_sprawdzany_korzen = tworzenieKopii(znajdzWezel(klucz));
        Wezel aktualny_rodzic_sprawdzanego_korzenia = znajdzRodzica(aktualnie_sprawdzany_korzen);
        Wezel aktualny_dziadek_sprawdzanego_korzenia = znajdzRodzica(aktualny_rodzic_sprawdzanego_korzenia);
        Wezel przyszly_rodzic = null;

        if(aktualny_rodzic_sprawdzanego_korzenia != null)
        {
            if(aktualny_rodzic_sprawdzanego_korzenia.lewy.klucz == aktualnie_sprawdzany_korzen.klucz)
            {
                if (aktualnie_sprawdzany_korzen.klucz == klucz)
                {
                    System.out.println("Szukany przez nas wezel: " + aktualnie_sprawdzany_korzen.klucz);
                    System.out.println("Rodzic naszego wezla: " + aktualny_rodzic_sprawdzanego_korzenia.klucz);
                    przyszly_rodzic = aktualnie_sprawdzany_korzen;
                    Wezel x = aktualnie_sprawdzany_korzen.prawy;
                    przyszly_rodzic.prawy = aktualny_rodzic_sprawdzanego_korzenia;
                    aktualny_rodzic_sprawdzanego_korzenia.lewy = x;

                    if (aktualny_dziadek_sprawdzanego_korzenia != null)
                    {
                        System.out.println("Dziadek naszego wezla: " + aktualny_dziadek_sprawdzanego_korzenia.klucz);
                        if (aktualny_dziadek_sprawdzanego_korzenia.prawy == aktualny_rodzic_sprawdzanego_korzenia)
                            aktualny_dziadek_sprawdzanego_korzenia.prawy = przyszly_rodzic;
                        else aktualny_dziadek_sprawdzanego_korzenia.lewy = przyszly_rodzic;
                    }

                    if (aktualny_dziadek_sprawdzanego_korzenia == null)
                        newKorzen = przyszly_rodzic;
                }
                System.out.println("------------------------------------------------------------");
                System.out.println("\nRysunek BST po wykonaniu rotacji w prawo wzgledem wezla o kluczu rownym " + aktualnie_sprawdzany_korzen.klucz + ": \n\n");
                Rysuj(newKorzen,zakreCalkowity);
                System.out.println("\n------------------------------------------------------------");
            }
            else {
                System.out.println("\nPrzy uzyciu tego wezla nie mozna wykonac rotacji w prawo!");
                System.out.println("\n------------------------------------------------------------");
            }
        }
        else{
            System.out.println("Szukany przez nas wezel: " + aktualnie_sprawdzany_korzen.klucz);
            przyszly_rodzic = aktualnie_sprawdzany_korzen.lewy; // A = C
            Wezel x = aktualnie_sprawdzany_korzen.lewy.prawy; // F lewy od A
            przyszly_rodzic.prawy = aktualnie_sprawdzany_korzen;
            aktualnie_sprawdzany_korzen.lewy = x;
            newKorzen = przyszly_rodzic;
            System.out.println("------------------------------------------------------------");
            System.out.println("\nRysunek BST po wykonaniu rotacji w prawo wzgledem wezla o kluczu rownym " + aktualnie_sprawdzany_korzen.klucz + ": \n\n");
            Rysuj(newKorzen,zakreCalkowity);
            System.out.println("\n------------------------------------------------------------");
        }

    }

    //Metoda zwracajaca nam wezel o podanym przez nas kluczu
    //Dziala tak samo jak metoda void ZnajdzWezel(), tylko nie wypisuje nam sciezki i zwraca obiekt
    public static Wezel znajdzWezel(int klucz)
    {
        Wezel aktualnyWezel = newKorzen;

        while (aktualnyWezel.klucz != klucz)
        {
            if (aktualnyWezel.klucz < klucz)
            {
                if (aktualnyWezel.prawy != null)
                    aktualnyWezel = aktualnyWezel.prawy;
                else
                    return null;
            }
            else
            {
                if (aktualnyWezel.lewy != null)
                    aktualnyWezel = aktualnyWezel.lewy;
                else
                    return null;
            }
        }
        return aktualnyWezel;
    }

    //Funkcja zwracajaca nam obiekt w postaci rodzica naszego aktualnego wezla
    public static Wezel znajdzRodzica(Wezel dziecko)
    {
        //Jesli nie ma dziecka to zwracamy null
        if (dziecko == null)
            return null;

        //Inicjalizacja obiektow
        Wezel aktualnyWezel = newKorzen;
        Wezel rodzic = aktualnyWezel;


        //Petle wykonujemy dopoki klucz naszego wezla nie bedzie rowny kluczowi dziecka wprowadzonego do funckji
        while (aktualnyWezel.klucz != dziecko.klucz)
        {
            //Jesli klucz naszego wezla jest mniejszy od klucza przez nas podanego
            if (aktualnyWezel.klucz < dziecko.klucz)
            {
                //Sprawdzamy czy isnieje prawy syn naszego wezla
                if (aktualnyWezel.prawy != null)
                {
                    //Jesli tak to naszym aktualnym rodzicem jest nasz wezel
                    rodzic = aktualnyWezel;
                    //A naszym wezlem bedzie teraz jego prawy syn
                    aktualnyWezel = aktualnyWezel.prawy;
                }
                //Nie ma nasz wezel rodzica
                else
                    return null;
            }
            //Jesli klucz naszego wezla jest wiekszy od klucza przez nas podanego
            else
            {
                //I jesli mamy lewego syna
                if (aktualnyWezel.lewy != null)
                {
                    //To nasz rodzic jest rowny aktualnemu wezlowi
                    rodzic = aktualnyWezel;
                    //A naszym aktualnym wezelem jest teraz jego lewy syn
                    aktualnyWezel = aktualnyWezel.lewy;
                }
                //Nie ma nasz wezel rodzica
                else
                    return null;
            }
        }
        //Jesli aktualny wezel jest korzeniem
        if (aktualnyWezel == newKorzen)
            return null;

        //Zwracamy naszego aktualnego rodzica
        //Jest to rodzic wezla ktorego klucz podalismy na wejsciu funkcji
        return rodzic;
    }

    //Pomocnicza metoda pozwalajaca policzyc potege. Podajemy na wejsciu podstawe i wykladnik.
    static int potega(int N, int M)
    {
        int power = N, sum = 1;
        while(M > 0)
        {
            if((M & 1) == 1)
            {
                sum *= power;
            }
            power = power * power;
            M = M >> 1;
        }
        return sum;
    }

    //Balans drzewa BST
    //Wykorzystany algorytm z zajec z lekkimi modyfikacjami
    public static void DSW(){
        int licznik_wierszy = 0;
        Wezel aktualnyWezel = newKorzen;
        int liczba_obrotow;

        //Tworzenie listy w postaci prawej galezi drzewa
        while(aktualnyWezel != null){
            //Jesli aktualny wezel nie ma lewego syna
            if(aktualnyWezel.lewy == null)
            {
                //To zwiekszamy liczbe wierszy o 1
                licznik_wierszy = licznik_wierszy + 1;
                //I schodzimy o jeden wezel w dol w prawo
                aktualnyWezel = aktualnyWezel.prawy;
            }
            //Jesli aktualny wezel ma lewego syna
            else
            {
                //To wykonujemy rotacje wzgledem lewego syna w prawo
                rotacjaPrawa(aktualnyWezel.lewy.klucz);
                //Szukamy rodzica naszego syna ktorego bedziemy brac pod uwage przy kolejnym wywolaniu petli
                aktualnyWezel = znajdzRodzica(aktualnyWezel);
            }
        }

        //Wylicznie liczby obrotow w naszym drzewi
        //Liczymy logarytm z liczby wierszy+1
        double logs = Math.log(licznik_wierszy + 1);
        //Zaokraglamy otrzymana liczbe
        int IntValue = (int) Math.round(logs);
        //Podnosimy liczbe 2 do potegi z logarytmu
        int potega = potega(2,IntValue);
        //Przy pomocy powyzszych obliczen, obliczamy ilosc potrzebnych obrotow
        liczba_obrotow = licznik_wierszy + 1 - potega;

        //Zaczynamy od korzenia
        aktualnyWezel = newKorzen;
        //Pomocniczy wezel tymaczoswy
        Wezel tempWezel;
        //Obracamy korzen w lewo
        rotacjaLewa(aktualnyWezel.klucz);
        //Wezel tymczasowy jest teraz rodzicem naszego korzenia po rotacji w lewo, czyli tak naprawde to aktualny korzen
        tempWezel = znajdzRodzica(aktualnyWezel);
        //Schodzimy o 2 synow w prawo i wchodzimy do petli
        aktualnyWezel = tempWezel.prawy.prawy;
        for(int i = 1; i <= liczba_obrotow - 2; i++)
        {
            //Wykonujemy rotacje w lewo wzgledem aktualnego wezla
            rotacjaLewa(aktualnyWezel.klucz);
            //Nadpisujemy nasz wezel prawego wnuczka
            aktualnyWezel = aktualnyWezel.prawy.prawy;
            //Petle wykonujemy az dojedziemy do konca naszej prawej galezi
        }

        //Wyliczamy nowa liczbe wierszy potrzebna do kolejnych rotacji
        licznik_wierszy = licznik_wierszy - liczba_obrotow;

        //Rozpoczynamy znowu od rotacji wzgledem korzenia
        aktualnyWezel = newKorzen;
        rotacjaLewa(aktualnyWezel.klucz);
        //Szukamy rodzica naszego wezla
        tempWezel = znajdzRodzica(aktualnyWezel);
        //Dopoki licznik wierszy jest wiekszy od 1 wykonujemy petle
        while(licznik_wierszy > 1)
        {
            //Licznik wierszy przesuwamy bitowo o 1 w prawo
            licznik_wierszy = licznik_wierszy >> 1;
            //Schodzimy od aktualnego korzenia na prawego wnuczka
            aktualnyWezel = tempWezel.prawy.prawy;
            //Wykonujemy petle az i bedzie rowne liczbie wierszy
            for(int i = 1; i < licznik_wierszy; i ++)
            {
                //Wykonujemy rotacje w lewo wzgledem aktualnego wezla
                rotacjaLewa(aktualnyWezel.klucz);
                //Nadpisujemy aktualny wezel jako prawego wnuczka
                aktualnyWezel = aktualnyWezel.prawy.prawy;
            }
        }
        //Do pelnego balansu naszego drzewa potrzebna jest jeszcze rotacja w lewo wzgledem korzenia
        aktualnyWezel = newKorzen;
        //Po rotacji nasze drzewo powinno byc juz zbalansowane
        rotacjaLewa(aktualnyWezel.klucz);
        //Koniec
    }

    //Metoda pozwalajaca tworzyc kopie naszego drzewa pod innym adresem w pamieci
    public static Wezel tworzenieKopii(Wezel korzen)
    {
        if (korzen == null) return null;
        Wezel nowyWezel = new Wezel(korzen.klucz);
        nowyWezel.lewy = tworzenieKopii(korzen.lewy);
        nowyWezel.prawy = tworzenieKopii(korzen.prawy);
        return nowyWezel;
    }

    //Metoda pozwalajaca usuwac element naszego drzewa BST
    static Wezel usuwanieWezla(Wezel korzen, int klucz)
    {
        //Sprawdzamy czy nasze drzewo nie jest puste
        if (korzen == null)
            return korzen;

        //Porownujemy klucz ktory chcemy usunac z kluczem naszego korzenia
        //Jesli jest mniejszy
        if (klucz < korzen.klucz)
            //To wywolujemy nasza metode jeszcze raz tylko tym razem naszym korzeniem jest jego lewy syn - rekurencja
            korzen.lewy = usuwanieWezla(korzen.lewy, klucz);
        //Jesli jest wiekszy
        else if (klucz > korzen.klucz)
            //To wywolujemy nasza metode jeszcze raz tylko tym razem naszym korzeniem jest jego prawy syn - rekurencja
            korzen.prawy = usuwanieWezla(korzen.prawy, klucz);
        //Jesli nasze klucze sa sobie rowne to mozemy go usunac
        else {
            //Jesli korzen nie ma lewego syna to nadpisujemy go jego prawym synem
            if (korzen.lewy == null)
                return korzen.prawy;
            //Jesli korzeni ma prawego syna to nadpisujemy go jego lewym synem
            else if (korzen.prawy == null)
                return korzen.lewy;
            korzen.klucz = elementMinimalnyDrzewaBST(korzen.prawy);
            korzen.prawy = usuwanieWezla(korzen.prawy, korzen.klucz);
        }

        return korzen;
    }
}